package routes

import (
	"cidenet/cidenet/controllers"
	"github.com/gin-gonic/gin"
)

func Init() *gin.Engine {
	r :=gin.New()

	Routes(r)

	return r
}

func Routes(r *gin.Engine)   {
	v1 :=r.Group("/v1")
	{
		user := v1.Group("user")
		user.GET("",controllers.GetAll)
		user.GET("/filterUser",controllers.GetUser)
		user.POST("/create", controllers.CreateUser)

	}
}