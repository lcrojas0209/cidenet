package model

import (
	"cidenet/cidenet/shared"
	"errors"
	"fmt"
	"gorm.io/gorm"
)

type User struct {
	gorm.Model
	Name               string         `json:"name" db:"name" `
	Surname            string         `json:"surname" db:"surname"`
	SecondSurname      string         `json:"second_surname" db:"secondSurname"`
	Country            Country        `json:"country" gorm:"foreignkey:CountryID;association_autoupdate:false;association_autocreate:false"`
	CountryID          int64          `json:"country_id" db:"country"`
	Identifications    Identification `json:"country" gorm:"foreignkey:TypeIdentification;association_autoupdate:false;association_autocreate:false"`
	TypeIdentification int64          `json:"type_identification" db:"typeIdentification"`
	Credential         string         `json:"credential" db:"credential" `
	Email              string         `json:"email" db:"email"`
}

type Country struct {
	gorm.Model
	Name string `json:"name" db:"name" `
}

type Identification struct {
	gorm.Model
	Name string `json:"name" db:"name" `
}

type UserRequest struct {
	Name               string `json:"name"`
	Surname            string `json:"surname"`
	SecondSurname      string `json:"second_surname"`
	CountryName        string `json:"country_name"`
	TypeIdentification string `json:"type_identification"`
	Credential         string `json:"credential"`
	Email              string `json:"email"`
}

func CreateUserModel(user UserRequest) (User, error) {
	var createUser User

	createUser.Name = user.Name
	createUser.Surname = user.Surname
	createUser.SecondSurname = user.SecondSurname

	countries, err := GetCountry(user.CountryName)
	if err != nil {
		return createUser, errors.New("Pais no encontrado")
	}

	createUser.CountryID = int64(countries.ID)

	identification, errs := GetTypeIdentification(user.TypeIdentification)
	if errs != nil {
		return createUser, errors.New("Tipo de identificacion no es valido")
	}

	createUser.TypeIdentification = int64(identification.ID)
	createUser.Credential = user.Credential
	createUser.Email = user.Email

	er := shared.GetDb().Create(&createUser).Error
	if er != nil {
		return createUser, er
	}
	return createUser,nil
}

func GetAllModel() []User {
	var users []User

	shared.
		GetDb().Find(&users)

	return users
}

func GetUserModel(user UserRequest) ([]User, error) {
	users := []User{}
	fmt.Println(user.Name)

	splice := shared.GetDb()

	if user.Name != "" {
		splice = splice.Where("name = ?", user.Name)
	}

	if user.Surname != "" {
		splice = splice.Where("surname = ? ", user.Surname)
	}

	if user.SecondSurname != "" {
		splice = splice.Where("second_surname = ? ", user.SecondSurname)
	}

	if user.CountryName != "" {
		splice = splice.Where("country = ? ", user.CountryName)
	}

	splice = splice.Find(&users)

	if splice.RecordNotFound() {
		return users, errors.New("Usuario no encontrado")
	}
	return users, nil
}

func GetCountry(name string) (Country, error) {
	countries := Country{}

	if shared.GetDb().Where("name = ?",name).Find(&countries).RecordNotFound() {
		return countries, errors.New("Pais no encontrado ")
	}

	return countries, nil
}

func GetTypeIdentification(name string) (Identification, error) {
	ident := Identification{}

	if shared.GetDb().Where("name = ?",name).Find(&ident).RecordNotFound() {
		return ident, errors.New("Tipo de identificacion no es valido")
	}

	return ident, nil
}
