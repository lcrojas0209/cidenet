package controllers

import (
	"cidenet/cidenet/model"
	"cidenet/cidenet/services"
	"cidenet/cidenet/shared"
	"github.com/gin-gonic/gin"
	"net/http"
	"strings"
)

func CreateUser(c *gin.Context) {
	response := shared.Context{}
	userCreate := model.UserRequest{}

	if err := c.BindJSON(&userCreate); err != nil {
		c.JSON(http.StatusBadRequest, response.ResponseData("400", err.Error(), nil))
		return
	}

	userCreate.Name =strings.TrimSpace(userCreate.Name)
	userCreate.Surname =strings.TrimSpace(userCreate.Surname)
	userCreate.SecondSurname =strings.TrimSpace(userCreate.SecondSurname)
	userCreate.TypeIdentification =strings.TrimSpace(userCreate.TypeIdentification)
	userCreate.Email =strings.TrimSpace(userCreate.Email)
	userCreate.Credential =strings.TrimSpace(userCreate.Credential)
	userCreate.CountryName =strings.TrimSpace(userCreate.CountryName)

	user, err := services.CreateUserServices(userCreate)
	if err != nil {
		c.JSON(400, response.ResponseData(shared.StatusFail, err.Error(), ""))
	} else {
		c.JSON(200, response.ResponseData(shared.StatusSuccess,"",user))
	}

}

func GetAll(c *gin.Context)  {
	response := shared.Context{}

	users := services.GetAllUsersServices()

	c.JSON(200, response.ResponseData(shared.StatusSuccess,"",users))
	return
}

func GetUser (c *gin.Context)  {
	response := shared.Context{}
	userRequest := model.UserRequest{}

	if err := c.BindJSON(&userRequest); err != nil {
		c.JSON(http.StatusBadRequest, response.ResponseData("400", err.Error(), nil))
		return
	}

	user, err := services.GetUsersServices(userRequest)
	if err != nil {
		c.JSON(400, response.ResponseData(shared.StatusFail, err.Error(), ""))
	} else {
		c.JSON(200, response.ResponseData(shared.StatusSuccess,"",user))
	}
}