package shared

import (
	"fmt"
	"github.com/jinzhu/gorm"
	"github.com/lib/pq"
	"go.uber.org/zap"
	sqltrace "gopkg.in/DataDog/dd-trace-go.v1/contrib/database/sql"
	gormtrace "gopkg.in/DataDog/dd-trace-go.v1/contrib/jinzhu/gorm"
)

var BD *gorm.DB
var err error

func Init() {
	dbinfo := "host=localhost user=postgres password=LUIScamilo1 dbname=cidenet port=5432 sslmode=disable"

	fmt.Println(dbinfo)

	sqltrace.Register("postgres", &pq.Driver{}, sqltrace.WithServiceName("cidenet-bd"))

	BD, err = gormtrace.Open("postgres", dbinfo)
	if err != nil {
		zap.S().Info(err)
		zap.S().Panic(err)
		panic(err)
	}
}

func GetDb() *gorm.DB {
	return BD
}

func CloseDb() {
	BD.Close()
}
