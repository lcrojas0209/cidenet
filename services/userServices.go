package services

import (
	"cidenet/cidenet/model"
)

func GetAllUsersServices() []model.User {
	users := model.GetAllModel()
	return users
}

func CreateUserServices(user model.UserRequest) (model.User, error) {
	users, err := model.CreateUserModel(user)
	if err != nil {
		return users,err
	}

	return users, nil
}

func GetUsersServices(user model.UserRequest) ([]model.User,error)  {
	users, err := model.GetUserModel(user)
	if err != nil {
		return users,err
	}
	return users, nil
}