/*
 * Created by lrojas
*/

package main

import (
	"cidenet/cidenet/routes"
	database "cidenet/cidenet/shared"
)

func main() {

	r := routes.Init()

	database.Init()
	defer database.CloseDb()

	/*
	database.BD.AutoMigrate(&model.User{})
	database.BD.AutoMigrate(&model.Identification{})
	database.BD.AutoMigrate(&model.Country{})
	*/

	r.Run(":3000")
}


